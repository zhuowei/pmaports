# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdeclarative
pkgver=5.52.0
pkgrel=0
pkgdesc="Provides integration of QML and KDE Frameworks"
arch="all"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1"
depends=""
depends_dev="kpackage-dev kconfig-dev kiconthemes-dev kglobalaccel-dev kwindowsystem-dev
			 kio-dev kguiaddons-dev qt5-qtdeclarative-dev ki18n-dev kcoreaddons-dev kservice-dev
			 kbookmarks-dev kwidgetsaddons-dev kcompletion-dev kitemviews-dev kjobwidgets-dev
			 solid-dev kxmlgui-dev kconfigwidgets-dev kauth-dev kcodecs-dev libepoxy-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/${pkgname}-${pkgver}.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Fails due to requiring running X11

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DKDE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="${pkgdir}" install
}
sha512sums="3464a45e53ff1cb51b648946d908e8d0aefb3ac8a6fd233afaf5313c1aa265c1bcf27d6ec624388470efadac28e445ad4906371154adf6b099df377324f47f2e  kdeclarative-5.52.0.tar.xz"
